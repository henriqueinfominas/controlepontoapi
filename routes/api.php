<?php

use Illuminate\Support\Facades\Route;

################# middleware's ######################
use App\Http\Middleware\verificaGrupoUser;
#####################################################

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::group(['middleware' => 'auth:api'], function () {
    Route::post('details', 'API\UserController@details');
});
Route::prefix('user')->group(function () {
    Route::get('/lista', 'API\UserController@listaUsuarios');
    Route::post('/inserir', 'API\UserController@inserirDadosUsuario');
    
});
Route::prefix('registro')->group(function () {
    Route::get('/buscar', 'RegistroController@buscar');
    Route::post('/inserir', 'RegistroController@inserir');
});