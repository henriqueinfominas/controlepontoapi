<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="Comprovação Entregas">
    <title>Azapfy - Velocidade para fazer</title>
    <link rel="icon" type="image/x-icon" href="favicon.ico" />

    <link rel="stylesheet" href="assets/css/loading-inicial.css">
    <link rel="stylesheet" href="vendors/material-design-iconic-font/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="vendors/weather-icons/css/weather-icons.min.css">
    <link rel="stylesheet" href="vendors/flag/sprite-flags-24x24.css" />
    <link rel="stylesheet" href="vendors/animate.css">
    <link rel="stylesheet" href="vendors/bootstrap-rtl.css">
    <link rel="stylesheet" href="vendors/loader.css">
    <link rel="stylesheet" href="vendors/react-notification/react-notifications.css">
    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Azapfy - Velocidade Para Fazer">
    <link rel="apple-touch-icon-precomposed" href="favicon.ico">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>

<body id="body" class="dark-deep-orange">
    <div id="app-site">
        <div id="loading">
            <div style="bottom:0;left:0;overflow:hidden;position:absolute;right:0;top:0">
                <div style="animation:a-h .5s 1.25s 1 linear forwards,a-nt .6s 1.25s 1 cubic-bezier(0,0,.2,1);background:#eee;border-radius:50%;height:800px;left:50%;margin:-448px -400px 0;position:absolute;top:50%;transform:scale(0);width:800px"></div>
            </div>
            <div style="height:100%;text-align:center">
                <div style="height:50%;margin:0 0 -140px"></div>
                <div style="height:128px;margin:0 auto;position:relative;width:176px">
                <img src="assets/images/icone-azapfy-branco.png" alt="logo-azapfy" class="img-fluid animated zoomInDown animation-duration-3 animation-delay-20">
                </div>
                <div id="nlpt"></div>
                <div style="animation:a-s .25s 1.25s 1 forwards;opacity:0" class="msg">Carregando Azapfy&hellip;</div>
            </div>
        </div>
    </div>
    <script src="{{ asset('assets/app.js') }}"></script>

</body>

</html>
