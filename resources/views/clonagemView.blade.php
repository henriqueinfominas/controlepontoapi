<?php
//requerendo classes
require_once '../app/Http/Controllers/classes/ClonagemSefaz.php';


//pegando notas emitidas contra cnpj sem saber chave (com certificado Digital)
$clonagemLoteNotas = new ClonagemSefaz();
//declara contador de notas
$contadorAux = array();
$contador = array();
$contador["total"] = 0;
$contador["baixou"] = 0;
$contador["jaBaixado"] =0;
//inicia o loop
//result sempre recebe erro ou sucesso

$result = $clonagemLoteNotas->loop("07515777000138");
$contadorAux = $clonagemLoteNotas->getContador();
$contador["total"] +=  $contadorAux["total"];
$contador["baixou"] +=  $contadorAux["baixou"];
$contador["jaBaixado"] +=  $contadorAux["jaBaixado"];

while($result == "erro"){
    //libera a memória quando o site da sefaz da erro
    unset($clonagemLoteNotas);

    //espera 15s para começar o processo denovo
    sleep(15);

    $clonagemLoteNotas = new ClonagemSefaz();
    //se result receber erro, o processo continua
    $result = $clonagemLoteNotas->loop("07515777000138");
    $contadorAux = $clonagemLoteNotas->getContador();
    $contador["total"] +=  $contadorAux["total"];
    $contador["baixou"] +=  $contadorAux["baixou"];
    $contador["jaBaixado"] +=  $contadorAux["jaBaixado"];
}
print_r(json_encode($contador));
?>