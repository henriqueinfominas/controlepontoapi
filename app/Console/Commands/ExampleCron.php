<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Http\Controllers\InformacoesRomaneio;
use \Exception;
use PHPMailer\PHPMailer\PHPMailer;


class ExampleCron extends Command
{

    protected $signature = 'example:cron';

    protected $description = 'Command E-mail';

    public function __construct()
    {
        parent::__construct();
    }
    // aqui você coloca a lógica do seu processo
    // pode utilizar todos os recursos do Laravel

    public function handle()
    {
        $mail = new PHPMailer(true);

        try{
            //Enable SMTP debugging. 
            $mail->SMTPDebug = 3;                               
            //Set PHPMailer to use SMTP.
            $mail->isSMTP();            
            //Set SMTP host name                          
            $mail->Host = "mail.m9.network";
            //Set this to true if SMTP host requires authentication to send email
            $mail->SMTPAuth = true;                          
            //Provide username and password     
            $mail->Username = "suporte@azapfy.com.br";                 
            $mail->Password = "Infominaswrj133";                           
            //If SMTP requires TLS encryption then set it
            $mail->SMTPSecure = "tls";                           
            //Set TCP port to connect to 
            $mail->Port = 587;                                   
            
            $mail->From = "suporte@azapfy.com.br";
            $mail->FromName = "Romaneio Automatico";
            
            $mail->addAddress("filipeazapfy@gmail.com", "Azapfy");
            
            $mail->isHTML(true);
            
            $mail->Subject = "Teste execucao funcoes automaticas";
            $mail->Body = "".date("H:i",time())."";
            
            #Envio da Mensagem
            $enviado = $mail->Send();
                    
            #Limpa os destinatarios e os anexos
            $mail->ClearAllRecipients();
            $mail->ClearAttachments();

            if ($enviado) {
                echo "E-mail enviado com sucesso!";
            } 
        } catch (Exception $e) {
            echo array("resultado" => false, "erro" => array("codigo" => "1003", "erro" => $mail->ErrorInfo));
        }
    }
}