<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Jenssegers\Mongodb\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';

    // retorna quais campos sao obrigatorios para o motorista
    public function getObrigatorioMotorista()
    {
        return $this->obrigatoriosMotorista;
    }

    // retorna quais campos sao obrigatorios para o parceiro
    public function getObrigatorioParceiro()
    {
        return $this->obrigatoriosParceiro;
    }

    // retorna quais campos sao obrigatorios para os outros tipos de usuario
    public function getObrigatorioUsuario()
    {
        return $this->obrigatoriosUsuario;
    }

    //pegando chaves unicas
    public function getChaveUnica()
    {
        return $this->chaveUnica;
    }

    //pegando chaves compostas unicos
    public function getChaveComposta()
    {
        return $this->chaveComposta;
    }

    protected $fillable = [
        'remember_token',       // string 
        'name',                 // String
        'email',                // string  
        'cpf',                  // string  
        'ativo',                // Boolean,
        'avatar',                // string
    ];

    //chaves unicas
    protected $chaveUnica = [
        'email',
        'cpf'
    ];

    //chaves Compostas
    protected $chaveComposta = [];

    //campos ocultos para pesquisa
    protected $hidden = ['_id', 'updated_at', 'created_at', 'remember_token'];

    //moldes
    protected $casts = [];

    //datas
    protected $dates = ['created_at', 'updated_at','dt_token_app'];

    public $exists = true;
}