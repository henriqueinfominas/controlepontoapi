<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Registro extends Eloquent
{
  //collection
  protected $table = 'registro';

  //pegando obrigatorio
  public function getObrigatorioRegistros(){
    return $this->obrigatorios;
  }

  //pegando campos
  public function getFillable(){
    return $this->fillable;
  }

  //pegando chaves compostas unicos
  public function getChaveComposta()
  {
    return $this->chaveComposta;
  }

  //campos
  protected $fillable = [
    'tipo',     // string  
    'inicio',     // date 
    'fim',     // date
    'user',   // string 
    'ativo',   // Boolean  
    ];

  //campos obrigatorios
  protected $obrigatorios = [
    
  ];



  //chaves Compostas
  protected $chaveComposta = [];

  //campos ocultos em presquisas
  protected $hidden = ['_id', 'created_at', 'updated_at'];

  //moldes
  protected $casts = [];

  //datas
  protected $dates = [
    'created_at', 
    'updated_at', 
    'dt_emis_cte',
    'dt_validacao'
  ];

  public $exists = true;
}