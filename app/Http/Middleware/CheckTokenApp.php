<?php

namespace App\Http\Middleware;

use Closure;
use \Illuminate\Http\Response;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\Controller;

class CheckTokenApp
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userInstancia = new UserController();
        $controllerInstancia = new Controller();

        $dados = $request->all();
        try {
            //verifica cpf
            if (!isset($dados["cpf"])) {
                return response()->json(['resposta' => 'cpf nao enviado']);;
            }

            //pesquisa usuario pelo cpf
            $usuario = User::where("cpf", "=", $dados["cpf"])->first();
            if (empty($usuario)) {
                return response()->json(['resposta' => 'cpf incorreto']);;
            }
            $u = $usuario->toArray();
            
            if (isset($dados["token"])) {
                //chama função que descriptografa
                $token = $userInstancia->descriptografarToken($dados["token"], $dados["cpf"]);
              
                //verifica se o token esta correto
                if ($token["valor"] == $usuario->token_app) {
                    //verifica se o token tem menos de 24 hrs
                    if ($controllerInstancia->dateDiff("h", $u["dt_token_app"], date('Y-m-d H:i:s')) < 24) {
                        //tudo validado, prossegue com a requisição
                        return $next($request);
                    } else {
                        return response()->json(['resposta' => 'token_app expirado']);;
                    }

                } else {
                    return response()->json(['resposta' => 'token_app invalido']);
                }

            } else {
                return response()->json(['resposta' => 'token_app invalido']);
            }
        } catch (Exception $e) {
            return response()->json(['resposta' => $e->getMessage()]);
        }
    }

}
