<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use \Exception;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function createUser(array $data)
    {
        $resultado = Array();
        try{
            $usuario = [
                'name' => $data['name'],
                'email' => $data['email'],
                'cpf' => $data['cpf'],
                'password' => Hash::make($data['password']),
                'grupo_empresa' => $data['grupo_empresa'],
                'base' => $data['base'],
                'grupo_usuario' => $data['grupo_usuario'],
                'telefone' => $data['telefone']
            ];
    
            if($data['grupo_usuario'] == "motorista"){
                $usuario['cnh'] = $data['cnh'];
                $usuario['placa_veiculo'] = $data['placa_veiculo'];
                $usuario['tipo_veiculo'] = $data['tipo_veiculo'];
            }
    
            if($data['grupo_usuario'] == "parceiro"){
                $usuario['nome_empresa'] = $data['nome_empresa'];
                $usuario['cnpj_empresa'] = $data['cnpj_empresa'];
            }
            User::create($usuario);

            $resultado['resultado'] = true;
            return $resultado;
        }
        catch(Exception $e){
            $resultado['resultado'] = false;
            if(strstr( $e->getMessage(),'duplicate'))
            {
                if(strstr( $e->getMessage(),'email')){
                    $resultado["erro"] = array("codigo" => "617", "erro" => "E-mail ja cadastrado");
                }
                if(strstr( $e->getMessage(),'cpf')){
                    $resultado["erro"] =  array("codigo" => "618", "erro" => "CPF ja cadastrado");
                }
            } 
            else
            {
                $resultado["erro"] = array("codigo" => "619", "erro" => $e->getMessage());
            }
            return $resultado;
        }   
    }
}
