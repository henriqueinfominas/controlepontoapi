<?php

namespace App\Http\Controllers;

use App\Models\Registro;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use MongoDB\BSON\UTCDateTime;

class RegistroController extends Controller
{
 public function buscar (){
    $dados = Registro::all();
    return $dados;
 }
 public function inserir(Request $request){
   $dados = $request->all();
   $dados["ativo"] = true;
   $result = ["resultado"=> true];
    try{
      Registro::create($dados);
      return $result;
    }catch(Exception $e){
      $result = ["resultado"=> false, error => $e->get_error_cod()];
      return $result;
    }
   
 }
 public function atualizar(Request $request){
   $dados = $request->all();
   $result = [resultado=> true];
    try{
      Registro::updateOrCreate(["_id"=> $dados["_id"]],$dados);
      return $result;
    }catch(Exception $e){
      $result = [resultado=> false, error => $e->get_error_cod()];
      return $result;
    }
   
 }


}
