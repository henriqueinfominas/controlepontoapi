<?php

namespace App\Http\Controllers;

use \Exception;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Carbon\Carbon;
use App\Models\Destinatario;
use App\Models\Rota;
use App\Models\Frete;
use App\Models\Romaneio;
use App\Models\Empresa;
use App\Models\user;
use App\Models\Nota;
use PHPMailer\PHPMailer\PHPMailer;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function padronizaData($date, $tipo = null)
    {
        try {
            $formats = array(
                'd/m/Y',
                'dmY',
                'd-m-Y',
            );

            $outputFormat = "";

            if ($tipo == null) {
                $outputFormat = 'Y-m-d';
            } else if ($tipo == "dash") {
                $outputFormat = 'Ymd';
            }

            foreach ($formats as $format) {
                $dateObj = \DateTime::createFromFormat($format, $date);

                if ($dateObj !== false) {
                    break;
                }
            }

            if ($dateObj === false) {
                throw new \Exception('Data invalida ' . $date);
            }

            return $dateObj->format($outputFormat);
        } catch (Exception $e) {
                //retornando erro
            return array("codigo" => "xxx", "erro" => $e->getMessage());
        }
    }

    protected function transformaDataParaQuery($data)
    {
        $resultado = array();
        if (!isset($data['data_inicio'])) {
            $resultado["resultado"] = false;
            $resultado["valor"] = "Data inicio não foi enviado";
            return $resultado;
        }
        if (isset($data['data_fim'])) {
            //transforma data inicio para Tipo do mongo
            $dataInicio = new \DateTime($data['data_inicio']);
            //adicionar 3 horas, pois padrao do mongo é UTCy
            $dataInicio = $dataInicio->add(new \DateInterval('PT3H'));
            $dataInicio = new \MongoDB\BSON\UTCDateTime(($dataInicio)->getTimestamp() * 1000);

            //transforma data fim para Tipo do mongo
            $dataFim = new \DateTime($data['data_fim']);
            //adicionar 3 horas, pois padrao do mongo é UTC
            $dataFim = $dataFim->add(new \DateInterval('PT3H'));
            $dataFim = new \MongoDB\BSON\UTCDateTime(($dataFim)->getTimestamp() * 1000);

            $resultado["resultado"] = true;
            $resultado["data_inicio"] = $dataInicio;
            $resultado["data_fim"] = $dataFim;
        } else {
            //padronizando data de inicio
            $dataInicio = $this->padronizaData($data['data_inicio']);
            
            //convertendo data de inicio
            $dataInicio = new \DateTime($dataInicio);

            //adicionando 3 horas
            $dataInicio = $dataInicio->format('Y-m-d 03:00:00');

            //padronizando data de fim
            $dataFim = $this->padronizaData($data['data_inicio']);

            //convertendo date de fim
            $dataFim = new \DateTime($dataFim);

            //adicionando 1 dia
            $dataFim = $dataFim->add(new \DateInterval('P1D'));

            //adicionando 3 horas
            $dataFim = $dataFim->format('Y-m-d 03:00:00');

            //convertendo em milisegundos
            $dataInicio = new \MongoDB\BSON\UTCDateTime((new \DateTime($dataInicio))->getTimestamp() * 1000);
            $dataFim = new \MongoDB\BSON\UTCDateTime((new \DateTime($dataFim))->getTimestamp() * 1000);

            $resultado["resultado"] = true;
            $resultado["data_inicio"] = $dataInicio;
            $resultado["data_fim"] = $dataFim;
        }
        return $resultado;
    }

    protected function montaEndereco($endereco, $formata = false)
    {
        $campo = array();
        $resutado = array();
        $endereco = explode(",", $endereco);
        foreach ($endereco as $k => $valor) {
            $campo = explode(":", $valor);
            $resutado[$campo[0]] = $campo[1];
        }


        return $resutado;
    }

    protected function dataParaString($campo, $valor)
    {
        $dados = Nota::where($campo, '=', $valor)
            ->whereNotNull('historico_nota')
            ->get(['historico_nota']);
        foreach ($dados as $key => $valor) {
            foreach ($valor->historico_nota as $k => $v) {
                if (!is_string($v['dt_criacao'])) {
                    $data = substr($v['dt_criacao'], 0, -3);
                    $v['dt_criacao'] = Carbon::createFromTimestamp($data)->toDateTimeString();
                }
                $item[] = $v;
            }
            $valor->historico_nota = $item;
            $valor->save();
        }
    }

    public function datasCabon($dataIni = [], $dataFim = [])
    {

        try {
            foreach ($dataIni as $key => $valor) {

                switch (strlen($valor)) {
                    case 6:
                        $data[$key] = Carbon::createFromFormat('ymd', $valor, -2)->startOfDay();
                        break;
                    case 8:
                        $data[$key] = Carbon::createFromFormat('Ymd', $valor, -2)->startOfDay();
                        break;
                    case 10:
                        $data[$key] = Carbon::createFromFormat('YmdH', $valor, -2);
                        break;
                    case 12:
                        $data[$key] = Carbon::createFromFormat('YmdHi', $valor, -2);
                        break;
                    case 14:
                        $data[$key] = Carbon::createFromFormat('YmdHis', $valor, -2);
                        break;
                }
            }
            foreach ($dataFim as $key => $valor) {

                switch (strlen($valor)) {
                    case 6:
                        $data[$key] = Carbon::createFromFormat('ymd', $valor, -2)->endOfDay();
                        break;
                    case 8:
                        $data[$key] = Carbon::createFromFormat('Ymd', $valor, -2)->endOfDay();
                        break;
                    case 10:
                        $data[$key] = Carbon::createFromFormat('YmdH', $valor, -2);
                        break;
                    case 12:
                        $data[$key] = Carbon::createFromFormat('YmdHi', $valor, -2);
                        break;
                    case 14:
                        $data[$key] = Carbon::createFromFormat('YmdHis', $valor, -2);
                        break;
                }
            }
        } catch (Exception $e) {

            dd(['resposta' => 'Data(s) Invalida(s)']);
        }
        if (!empty($data)) {
            return $data;
        } else {

            dd(['resposta' => 'Data(s) Invalida(s)']);
        }

    }

    protected function verificaIndex($array, $opcionais, $collection)
    {
        //inicia variavel
        $resultado = array();
        //pega instancia da model
        $instancia = $this->getInstancia($collection);
        //se existe
        if ($instancia['resultado']) {
            //busca o Fillable
            $indexs = $instancia['instancia'];
            $indexs = $indexs->getFillable();
            //percorre o array passado
            foreach ($array as $k => $v) {
                if (!in_array($k, $indexs)) {
                    if (!in_array($k, $opcionais)) {
                        $resultado['resultado'] = false;
                        $resultado['erro'] = "O index " . $k . " nao existe";
                        return $resultado;
                    }
                }
                $resultado['resultado'] = true;
            }
        //se não achou instancia
        } else {
            $resultado['resultado'] = false;
            $resultado['erro'] = $instancia['instancia'];
        }
        return $resultado;
    }

    protected function verificaObrigatorios($array, $collection)
    {
        //inicia variavies
        $resultado = array();
        //pega instancia da model
        $instancia = $this->getInstancia($collection);
         //se existe
        if ($instancia['resultado']) {
            $indexs = $instancia['instancia'];
            //busca os obrigatorios
            $indexs = $indexs->getObrigatorio();
            foreach ($indexs as $obrigatorio) {
                if (array_key_exists($obrigatorio, $array)) {
                    $resultado['resultado'] = true;
                } else {
                    $resultado['resultado'] = false;
                    $resultado["erro"] = "O index " . $obrigatorio . " nao foi encontrado";
                    return $resultado;
                }
            }
        //se não achou instancia
        } else {
            $resultado['resultado'] = false;
            $resultado['erro'] = $instancia['instancia'];
        }
        return $resultado;
    }

    protected function getInstancia($collection)
    {
        $resultado = array();
        switch ($collection) {
            case "Rota":
                $resultado['resultado'] = true;
                $instancia = new Rota;
                break;
            case "Romaneio":
                $resultado['resultado'] = true;
                $instancia = new Romaneio;
                break;
            case "Destinatario":
                $resultado['resultado'] = true;
                $instancia = new Destinatario;
                break;
            case "Cte":
                $resultado['resultado'] = true;
                $instancia = new Cte;
                break;
            case "Frete":
                $resultado['resultado'] = true;
                $instancia = new Frete;
                break;
            case "Nota":
                $resultado['resultado'] = true;
                $instancia = new Nota;
                break;
            case "User":
                $resultado['resultado'] = true;
                $instancia = new User;
                break;
            case "Empresa":
                $resultado['resultado'] = true;
                $instancia = new Empresa;
                break;
            default:
                $resultado['resultado'] = false;
                $instancia = "Nao foi possivel instanciar " . $collection;
        }

        $resultado['instancia'] = $instancia;
        return $resultado;
    }

    function buscarGeo($address, $key = 'AIzaSyB0Uj6Vph7m8RKyQuk76WevZsN5tbuZW_Q')
    {    
        //Funcao que retorna a latitude e a logitude de qualque endereco passado por parametro. 
        $url = sprintf('https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=%s', urlencode($address), urlencode($key));
        $response = file_get_contents($url);
        $data = json_decode($response);
        if ($data->status == 'OK') {
            return $data->results[0]->geometry->location;
        }
        return false;
    }

    //gerando protocolo
    public function geraProtocolo($cnpj_tomador)
    {
        try {
            //pega data atual
            $data = date('Ymdhis');

            //gera protocolo
            $protocolo = $data . substr($cnpj_tomador, 0, 3) . substr($cnpj_tomador, -3);

            //retorna protocolo
            return $protocolo;
        } catch (\Exception $e) {
            //retornando erro
            return array("codigo" => "325", "erro" => $e->getMessage());
        }
    }

    public function numeroAr($chave)
    {
        try {
            //ano e mes
            $data = substr($chave, 2, 4);

            //cnpj do emitente
            $cnpj = substr($chave, 6, 14);
            
            //numero
            $numero = substr($chave, 25, 9);

            //retornando numero do ar montando
            return (string)$data . $cnpj . $numero;
        } catch (\Exception $e) {
            //retornando erro
            return array("codigo" => "324", "erro" => $e->getMessage());
        }
    }

    //montando numero ar a partir de data cnpj e numero
    public function montaNumeroAr($data, $cnpj, $numero)
    {
        try {
            //pegando ano e mes data
            $data = date("ym", strtotime($data));

            //numero com zero a esquerda
            $numero = str_pad($numero, 9, '0', STR_PAD_LEFT);

            //retornando numero ar
            return (string)$data . $cnpj . $numero;
        } catch (\Exception $e) {
            //retornando erro
            return array("codigo" => "xxx", "erro" => $e->getMessage());
        }
    }

    public function transformarCaracteresHtml($string){
        $antes = array("ç","í","á", "ã","õ","ú","à","ó","é","ê", "â","ô","É","Á","À");
        $depois = array("&ccedil;","&iacute;","&aacute;","&atilde;","&otilde;","&uacute;","&agrave;",
                        "&oacute;","&eacute;","&ecirc;","&acirc;","&ocirc;","&Eacute;","&Aacute;","&Agrave;");
        return str_replace($antes, $depois, $string);
    }

    public function removerCaracteresEspeciais($string)
    {
        // matriz de entrada
        $what = array('ä', 'ã', 'à', 'á', 'â', 'ê', 'ë', 'è', 'é', 'ï', 'ì', 'í', 'ö', 'õ', 'ò', 'ó', 'ô', 'ü', 'ù', 'ú', 'û', 'À', 'Á', 'É', 'Í', 'Ó', 'Ú', 'ñ', 'Ñ', 'ç', 'Ç', ';', ':', '|', '!', '"', '#', '$', '%', '&', '/', '=', '?', '~', '^', '>', '<', 'ª', 'º', "'");
        // matriz de saída
        $by = array('a', 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'A', 'A', 'E', 'I', 'O', 'U', 'n', 'N', 'c', 'C', '', '', '', '', '', '', '', '', 'E', '', '', '', '', '', '', '', '', '', '');
        // devolver a string
        return str_replace($what, $by, $string);
    }

    public function formataApp($string)
    {
        $string = strtolower($string);
        $what = array('ä', 'ã', 'à', 'á', 'â', 'ê', 'ë', 'è', 'é', 'ï', 'ì', 'í', 'ö', 'õ', 'ò', 'ó', 'ô', 'ü', 'ù', 'ú', 'û', 'À', 'Á', 'É', 'Í', 'Ó', 'Ú', 'ñ', 'Ñ', 'ç', 'Ç', ';', ':', '|', '!', '"', '#', '$', '%', '&', '/', '=', '?', '~', '^', '>', '<', 'ª', 'º', "'", " ");
        // matriz de saída
        $by = array('a', 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'A', 'A', 'E', 'I', 'O', 'U', 'n', 'N', 'c', 'C', '', '', '', '', '', '', '', '', 'E', '', '', '', '', '', '', '', '', '', '', "_");
        // devolver a string
        return str_replace($what, $by, $string);
    }

    ###FUNCAO para mapear qualquer array, requer dois arrays, um de mapeamento outro com dados
    /*EXEMPLO:  $mapeamento = [
        'romaneio_transp' => 'romaneio',
        'data_romt' => 'data_romaneio',
        'end_dest' => 'endereco_destinatario',
    ];
     */

    public function mapeamento($mapeamento, $dados)
    {
        $dados = json_encode($dados);
        foreach ($mapeamento as $antes => $depois) {
            $dados = str_replace('"' . $antes . '":', '"' . $depois . '":', $dados);
        }
        return json_decode($dados);
    }

    public function enviaEmailNota(Request $request)
    {
        $arrayDados = $request->all();
        //PHPMailer Object
        $mail = new PHPMailer(true);

        try {
            //Enable SMTP debugging.
            $mail->SMTPDebug = 3;
            //Set PHPMailer to use SMTP.
            $mail->isSMTP();
            //Set SMTP host name
            $mail->Host = "mail.m9.network";
            //Set this to true if SMTP host requires authentication to send email
            $mail->SMTPAuth = true;
            //Provide username and password
            $mail->Username = "suporte@azapfy.com.br";
            $mail->Password = "Infominaswrj133";
            //If SMTP requires TLS encryption then set it
            $mail->SMTPSecure = "tls";
            //Set TCP port to connect to
            $mail->Port = 587;

            $mail->From = "suporte@azapfy.com.br";
            $mail->FromName = $arrayDados["nome_remetente"];

            foreach ($arrayDados["destinatario"] as $item => $email) {
                $mail->addAddress($email);
            }

            $mail->addReplyTo($arrayDados["remetente"], $arrayDados["nome_remetente"]);

            $mail->isHTML(true);

            $mail->Subject = $arrayDados["assunto"];

            $mail->Body = "<p>".$arrayDados["texto"]."</p><br><br><br>";

            foreach ($arrayDados["notas"] as $item => $nota) {
                   //montando url
                   //dd("aqui");
                   $url[$item] = Storage::disk('s3')->temporaryUrl($nota, now()->addDays(7));
            }
            if(isset($url)){
                if(count($url) < 2){
                   $mail->Body .= "<b>Imagem solicitada no Sistema Azapfy: </b><br><br><br><img src=".$url[0]." width=300 height=400><br><br><ul><li><u>Obs: A imagem está disponível por 7 dias. Caso necessite, realize o download ou solicite novamente após esse período.</u></li></ul><br>";
                } else {
                   $mail->Body .= "<b>Imagens solicitadas no Sistema Azapfy: </b><br><br><br>";
                   foreach($url as $key => $value){
                       $mail->Body .= "<img src=".$value." width=300 height=400><br><br>";
                   }
                   $mail->Body .=  "<br><ul><li><u>Obs: As imagens estão disponiveís por 7 dias. Caso necessite, realize o download ou solicite novamente após esse período.</u></li></ul><br>";
                }
                $mail->Body .= '<hr size="1" />
                                <table>
                                    <tr> 
                                        <td>
                                            <a href="https://www.azapfy.com.br/">
                                                <img src="https://yt3.ggpht.com/a-/AAuE7mC-pCpsdeei-koaWM_bCus2CD0Q7-Wa0ZtlKQ=s288-mo-c-c0xffffffff-rj-k-no" border="0" width=60 height=60 />
                                            </a>
                                        </td>
                                        <td>
                                            <p>'.$arrayDados["nome_remetente"].' - '.$arrayDados['grupo_empresa'].'</p>
                                            <strong>E-mail enviado via Sistema Azapfy</strong>
                                        </td> 
                                    </tr>
                                </table>
                            <hr size="1" />';
            }

            $mail->Body = $this->transformarCaracteresHtml($mail->Body);

            #Envio da Mensagem
            $enviado = $mail->Send();

            #Limpa os destinatarios e os anexos
            $mail->ClearAllRecipients();
            $mail->ClearAttachments();

            if ($enviado) {
                return "E-mail enviado com sucesso!";
            }

        } catch (Exception $e) {
            return array("resultado" => false, "erro" => array("codigo" => "1003", "erro" => $mail->ErrorInfo));
        }
    }

    //pega objeto da imagem no s3
    public function getFileInS3($caminho_s3)
    {
        try
        {
            //verificando se existe
            $verificaFile = Storage::disk('s3')->exists($caminho_s3);

            //verificando resposta
            if ($verificaFile == 1) {
                //pegando no s3
                $objImage = Storage::disk('s3')->get($caminho_s3);
            } else {
                //nova exception
                throw new \Exception("Arquivo nao encontrado no servidor");
            }

            //retornando objeto 
            return $objImage;
        } catch (\Exception $e) {
            //retornando erro
            return array("codigo" => "707", "erro" => $e->getMessage());
        }
    }

    public static function dateDiff($str_interval, $dt_menor, $dt_maior, $relative = false)
    {
        if (is_string($dt_menor)) $dt_menor = date_create($dt_menor);
        if (is_string($dt_maior)) $dt_maior = date_create($dt_maior);

        $diff = date_diff($dt_menor, $dt_maior, !$relative);

        $total = 0;

        switch ($str_interval) {
            case "y":
                $total = $diff->y + $diff->m / 12 + $diff->d / 365.25;
                $total = round($total, 1);
                break;
            case "m":
                $total = $diff->y * 12 + $diff->m + $diff->d / 30 + $diff->h / 24;
                $total = round($total, 1);
                break;
            case "d":
                $total = $diff->y * 365.25 + $diff->m * 30 + $diff->d + $diff->h / 24 + $diff->i / 60;
                $total = round($total, 1);
                break;
            case "h":
                $total = ($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h + $diff->i / 60;
                $total = round($total, 2);
                break;
            case "i":
                $total = (($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i + $diff->s / 60;
                $total = round($total, 2);
                break;
            case "s":
                $total = ((($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i) * 60 + $diff->s;
                $total = round($total, 2);
                break;
        }

        if ($diff->invert)
            return -1 * $total;
        else
            return $total;
    }
}