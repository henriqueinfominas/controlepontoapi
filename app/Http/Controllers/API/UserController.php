<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Nota;
use App\Models\Romaneio;
use App\Models\Rota;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use \Exception;

class UserController extends Controller
{
    public $successStatus = 200;
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        $dadosLogin['cpf'] = request('cpf');
        $dadosLogin['email'] = request('email');
        $dadosLogin['password'] = request('password');

        $pesquisa = $this->pesquisarUsuario($dadosLogin['cpf']);
        if ($pesquisa['resultado'] == false) {
            $pesquisa = $this->pesquisarEmail($dadosLogin['email']);
        }

        if ($pesquisa['resultado'] == true) {
            if (!isset($pesquisa['valor'][0]['password'])) {
                if (isset($dadosLogin['password'])) {
                    try {
                        $cadastroSenha = $this->cadastroSenha($pesquisa["valor"][0]["cpf"], $dadosLogin['password']);
                    } catch (Exception $e) {
                        $resultado['resultado'] = false;
                        $resultado['erro'] = $cadastroSenha;
                        return response()->json([$resultado, 'email' => $pesquisa["valor"][0]["email"], 'cpf' => $pesquisa["valor"][0]["cpf"]], 401);
                    }
                } else {
                    return response()->json(['erro' => 'Usuario sem senha cadastrada', 'email' => $pesquisa["valor"][0]["email"], 'cpf' => $pesquisa["valor"][0]["cpf"]], 401);
                }
            }
            if (isset($pesquisa['valor'][0]['ativo']) && $pesquisa['valor'][0]['ativo'] == false) {
                return response()->json(['erro' => 'Usuario inativo'], 401);
            }
        }

        if (Auth::attempt(array_filter($dadosLogin))) {
            $user = Auth::user();
            $dadosAcesso = $this->retornaAcessoCpf($user->cpf);
            $success['token'] = $user->createToken('MyApp')->accessToken;
            $success['data'] = $user;
            return response()->json(['acessos' => $dadosAcesso, 'user' => $success], $this->successStatus);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] = $user->createToken('MyApp')->accessToken;
        $success['data'] = $user;
        return response()->json(['user' => $success], $this->successStatus);
    }

    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }

    public function buscarMotoristas($cnpj_empresa)
    {
        $motoristas = User::where("cnpj_empresa", "=", $cnpj_empresa)->where("grupo_usuario", "=", "motorista")->orderBy('name', 'asc')->get(["name", "email", "cpf", "grupo_emp", "cnpj_empresa", "base", "cnh", "placa_veiculo", "tipo_veiculo", "grupo_usuario"])->toArray();
        return $motoristas;
    }

    /**
     *  Verifica o tipo de usuario
     *  Encaminha para a funcao do cadastro do tipo de usuario especifico
     */
    public function cadastroUsuario(Request $request)
    {
        try
        {
            //pegando os dados
            $arrayDados = $request->all();

            //array de retorno
            $arrayRetorno = array();

            //pegando tipo de usuario
            $tipo_usuario = $arrayDados["grupo_usuario"];

            //verificando tipo de usuario motorista
            if ($tipo_usuario == "motorista") {
                //chamando a funcao de cadastro do motorista
                $cadastroMotorista = $this->cadastroMotorista($arrayDados);

                if (!$cadastroMotorista['resultado']) {
                    //retornando erro
                    return $cadastroMotorista;
                }

                //retornando resposta
                $arrayRetorno["resultado"] = $cadastroMotorista["resultado"];
                $arrayRetorno["valor"] = $cadastroMotorista["valor"];
            } else if ($tipo_usuario == "parceiro") {
                //chamando a funcao de cadastro do parceiro
                $cadastroParceiro = $this->cadastroParceiro($arrayDados);

                if (!$cadastroParceiro['resultado']) {
                    //retornando erro
                    return $cadastroParceiro;
                }
                //retornando resposta
                $arrayRetorno["resultado"] = $cadastroParceiro["resultado"];
                $arrayRetorno["valor"] = $cadastroParceiro["valor"];
            } else if ($tipo_usuario == "admin" || $tipo_usuario == "gestor" || $tipo_usuario == "visualizador" || $tipo_usuario == "cliente") {
                //chamando a funcao de cadastro do usuario
                $cadastroUsuarioComum = $this->cadastroUsuarioComum($arrayDados);

                if (!$cadastroUsuarioComum['resultado']) {
                    //retornando erro
                    return $cadastroUsuarioComum;
                }
                //retornando resposta
                $arrayRetorno["resultado"] = $cadastroUsuarioComum["resultado"];
                $arrayRetorno["valor"] = $cadastroUsuarioComum["valor"];
            } else {
                //nova exception
                return array("codigo" => "627", "erro" => 'Tipo de usuario inexistente.');
            }
            //retornando array
            return $arrayRetorno;
        } catch (Exception $e) {
            //retornando erro
            return array("resultado" => false, "erro" => array("codigo" => "601", "erro" => $e->getMessage()));
        }
    }

    /**
     *  Verifica se os dados obrigatorios do motorista estao preenchidos
     *  Encaminha para a insercao no sistema
     */
    public function cadastroMotorista($dadosMotorista)
    {
        try
        {
            //instanciando model
            $user = new User();

            $array_retorno = array();

            //pegando obrigatorios para motorista
            $obrigatoriosMotorista = $user->getObrigatorioMotorista();

            //verificando se esta faltando campos
            $verificaObrigatorios = $this->verificarObrigatorio($dadosMotorista, $obrigatoriosMotorista);
            //verificando se retornou true
            if ($verificaObrigatorios['resultado'] == true) {
                //cadastrando usuario motorista
                $inserirDadosUsuario = $this->inserirDadosUsuario($dadosMotorista);

                //verificando resposta
                if ($inserirDadosUsuario['resultado'] == true) {
                    $array_retorno["resultado"] = true;
                    $array_retorno["valor"] = array("usuario" => $dadosMotorista["name"], "status" => "salvo com sucesso");
                    //retornando
                    return $array_retorno;
                } else {
                    //retornando erro
                    return $inserirDadosUsuario;
                }
            } else {
                //retornando erro
                return $verificaObrigatorios;
            }
        } catch (\Exception $e) {
            //retornando erro
            return array("resultado" => false, "erro" => array("codigo" => "602", "erro" => $e->getMessage()));
        }
    }

    /**
     *  Verifica se os dados obrigatorios do usuario comum estao preenchidos
     *  Encaminha para a insercao no sistema
     */
    public function cadastroUsuarioComum($dadosUsuario)
    {
        try
        {
            //instanciando model
            $user = new User();

            $array_retorno = array();

            //pegando obrigatorios para o usuario
            $obrigatoriosUsuario = $user->getObrigatorioUsuario();

            //verificando se esta faltando campos
            $verificaObrigatorios = $this->verificarObrigatorio($dadosUsuario, $obrigatoriosUsuario);

            //verificando se retornou true
            if ($verificaObrigatorios['resultado'] == true) {
                //cadastrando usuario
                $inserirDadosUsuario = $this->inserirDadosUsuario($dadosUsuario);

                //verificando resposta
                if ($inserirDadosUsuario['resultado'] == true) {
                    $array_retorno["resultado"] = true;
                    $array_retorno["valor"] = array("usuario" => $dadosUsuario["name"], "status" => "salvo com sucesso");
                    //retornando
                    return $array_retorno;
                } else {
                    //retornando erro
                    return $inserirDadosUsuario;
                }
            } else {
                //retornando erro
                return $verificaObrigatorios;
            }
        } catch (\Exception $e) {
            //retornando erro
            return array("resultado" => false, "erro" => array("codigo" => "603", "erro" => $e->getMessage()));
        }
    }

    /**
     *  Verifica se os dados obrigatorios do parceiro estao preenchidos
     *  Encaminha para a insercao no sistema
     */
    public function cadastroParceiro($dadosParceiro)
    {
        try
        {
            //instanciando model
            $user = new User();

            $array_retorno = array();

            //pegando obrigatorios para o parceiro
            $obrigatoriosParceiro = $user->getObrigatorioParceiro();

            //verificando se esta faltando campos
            $verificaObrigatorios = $this->verificarObrigatorio($dadosParceiro, $obrigatoriosParceiro);

            //verificando se retornou true
            if ($verificaObrigatorios['resultado'] == true) {
                //cadastrando usuario parceiro
                $inserirDadosParceiro = $this->inserirDadosUsuario($dadosParceiro);

                //verificando resposta
                if ($inserirDadosParceiro['resultado'] == true) {
                    $array_retorno["resultado"] = true;
                    $array_retorno["valor"] = array("usuario" => $dadosParceiro["name"], "status" => "salvo com sucesso");
                    // retornando
                    return $array_retorno;
                } else {
                    //retornando erro
                    return $inserirDadosParceiro;
                }
            } else {
                //retornando erro
                return $verificaObrigatorios;
            }
        } catch (\Exception $e) {
            //retornando erro
            return array("resultado" => false, "erro" => array("codigo" => "604", "erro" => $e->getMessage()));
        }
    }

    /**
     * Verifica se todos os dados obrigatorios foram passados corretamente
     */
    public function verificarObrigatorio($arrayDados, $arrayObrigatorios)
    {
        try
        {
            //retorno true
            $resultado['resultado'] = true;

            //percorre cada chave
            foreach ($arrayObrigatorios as $key) {
                //verificando se nao existe chave no array de dados
                if (!array_key_exists($key, $arrayDados)) {
                    $resultado['resultado'] = false;
                    $resultado['erro'] = 'Campo ' . $key . ' e obrigatorio.';
                    return $resultado;
                }
            }
            //resposta do metodo
            return $resultado;
        } catch (\Exception $e) {
            //retornando erro
            return array("resultado" => false, "erro" => array("codigo" => "605", "erro" => $e->getMessage()));
        }
    }

    /**
     *  Realiza a insercao do novo usuario no sistema
     */
    public function inserirDadosUsuario(Request $request)
    {
        $resultado = array();
        $arrayDados = $request->all();
        try {
            //preenchendo array para cadastro do usuario
            $usuario = [
                'name' => $arrayDados['name'],
                'email' => $arrayDados['email'] ? $arrayDados['email'] : '',
                'cpf' => $arrayDados['cpf'],
                // 'grupo_usuario' => $arrayDados['grupo_usuario'] ? $arrayDados['grupo_usuario'] : 1,
                // 'telefone' => $arrayDados['telefone'] ? $arrayDados['telefone'] : '',
            ];

            if (isset($arrayDados['password'])) {
                $usuario['password'] = Hash::make($arrayDados['password']);
            }

            $usuario['ativo'] = true;

            User::create($usuario);

            $resultado['resultado'] = true;
            return $resultado;

        } catch (Exception $e) {
            $resultado['resultado'] = false;
            if (strstr($e->getMessage(), 'duplicate')) {
                if (strstr($e->getMessage(), 'email')) {
                    $resultado["erro"] = array("codigo" => "606", "erro" => "E-mail ja cadastrado");
                }
                if (strstr($e->getMessage(), 'cpf')) {
                    $resultado["erro"] = array("codigo" => "607", "erro" => "CPF ja cadastrado");
                }
            } else {
                $resultado["erro"] = array("codigo" => "608", "erro" => $e->getMessage());
            }
            return $resultado;
        }
    }

    /**
     *  Realiza a verificacao do cpf
     *  Encaminha para a atualizacao no sistema
     */
    public function editarUsuario(Request $request)
    {
        try
        {
            //array de retorno
            $arrayRetorno = array();

            //pegando os dados
            $arrayDados = $request->all();

            //pegando cpf
            $cpf_busca = $arrayDados["cpf_busca"];

            unset($arrayDados["cpf_busca"]);

            try {
                //atualizando
                if (array_key_exists("cpf", $arrayDados)) {
                    $cpf = $arrayDados["cpf"];
                    $update = $this->updateUsuario($cpf_busca, $arrayDados, $cpf);
                } else {
                    $update = $this->updateUsuario($cpf_busca, $arrayDados);
                }

                //verificando se nao houve erro
                if ($update['resultado'] == true) {

                    //pegando resultado
                    $arrayRetorno["resultado"] = true;

                    if (isset($update['valor'])) {
                        if (isset($cpf)) {
                            $arrayRetorno["valor"] = array("cpf" => $cpf, "status" => "atualizado com sucesso", "informacao" => "" . $update['valor'] . "");
                        } else {
                            $arrayRetorno["valor"] = array("cpf" => $cpf_busca, "status" => "atualizado com sucesso", "informacao" => "" . $update['valor'] . "");
                        }
                    } else {
                        if (isset($cpf)) {
                            $arrayRetorno["valor"] = array("cpf" => $cpf, "status" => "atualizado com sucesso");
                        } else {
                            $arrayRetorno["valor"] = array("cpf" => $cpf_busca, "status" => "atualizado com sucesso");
                        }
                    }
                } else {
                    //retornando resposta
                    return $update;
                }
                //retornando array
                return $arrayRetorno;
            } catch (\Exception $e) {
                return array("resultado" => false, "erro" => array("codigo" => "611", "erro" => $e->getMessage()));
            }
        } catch (\Exception $e) {
            if (strstr($e->getMessage(), 'Undefined index: cpf_busca ')) {
                //retornando erro
                return array("resultado" => false, "erro" => array("codigo" => "609", "erro" => "CPF nao informado"));
            } else {
                return array("resultado" => false, "erro" => array("codigo" => "610", "erro" => $e->getMessage()));
            }
        }
    }

    /**
     *  Realiza  a atualizacao do usuario no sistema
     */
    public function updateUsuario($cpf_busca, $arrayDados, $cpf = null)
    {
        try
        {
            $resultado['resultado'] = true;
            $arrayUpdate = array();
            $arrayRotas = array();
            $updateNotas = 0;
            $updateRomaneios = 0;
            $updateRotas = 0;

            //criptografando a senha
            if (array_key_exists("password", $arrayDados)) {
                $arrayDados['password'] = Hash::make($arrayDados['password']);
            }

            try {
                $pesquisa = User::where("cpf", "=", $cpf_busca)->get(['name', 'grupo_usuario'])->toArray();

                if ($pesquisa[0]['grupo_usuario'] == "motorista") {
                    if ($cpf != null) {
                        if (isset($arrayDados['name'])) {
                            $arrayUpdate = array('nome_motorista' => $arrayDados['name'], 'cpf_motorista' => $cpf);
                            $arrayRotas = array('nome_motorista_rota' => $arrayDados['name'], 'cpf_motorista_rota' => $cpf);
                        } else {
                            $arrayUpdate = array('cpf_motorista' => $cpf);
                            $arrayRotas = array('cpf_motorista_rota' => $cpf);
                        }
                    } else {
                        if (isset($arrayDados['name'])) {
                            $arrayUpdate = array('nome_motorista' => $arrayDados['name']);
                            $arrayRotas = array('nome_motorista_rota' => $arrayDados['name']);
                        }
                    }

                    $updateNotas = Nota::where("cpf_motorista", "=", $cpf_busca)->update($arrayUpdate);
                    $updateRotas = Rota::where("cpf_motorista_rota", "=", $cpf_busca)->update($arrayRotas);
                    $updateRomaneios = Romaneio::where("cpf_motorista", "=", $cpf_busca)->update($arrayUpdate);

                    if ($updateNotas > 0 || $updateRomaneios > 0 || $updateRotas > 0) {
                        $resultado['valor'] = 'Dados do motorista foram atualizados em outras tabelas';
                    }
                }
            } catch (\Exception $e) {
                if (strstr($e->getMessage(), 'Undefined')) {
                    if (strstr($e->getMessage(), 'offset: 0')) {
                        return array("resultado" => false, "erro" => array("codigo" => "642", "erro" => "CPF nao cadastrado"));
                    } else if (strstr($e->getMessage(), 'index')) {
                        return array("resultado" => false, "erro" => array("codigo" => "643", "erro" => "Dados obrigatorios nao cadastrados"));
                    }
                } else {
                    return array("resultado" => false, "erro" => array("codigo" => "637", "erro" => $e->getMessage()));
                }
            }
            //atualizando
            $update = User::where("cpf", "=", $cpf_busca)->update($arrayDados);

            //se atualizou
            if ($update == 1) {
                //retornando
                return $resultado;
            } else {
                $resultado['resultado'] = false;
                $resultado['erro'] = array("codigo" => "628", "erro" => 'CPF nao cadastrado');
                return $resultado;
            }
        } catch (\Exception $e) {
            $resultado['resultado'] = false;
            //retornando erro
            if (strstr($e->getMessage(), 'duplicate')) {
                if (strstr($e->getMessage(), 'email')) {
                    return array("resultado" => false, "erro" => array("codigo" => "611", "erro" => 'Email ja cadastrado'));
                }
                if (strstr($e->getMessage(), 'cpf')) {
                    return array("resultado" => false, "erro" => array("codigo" => "639", "erro" => 'CPF ja cadastrado'));
                }
            } else {
                //retornando erro
                return array("resultado" => false, "erro" => array("codigo" => "613", "erro" => $e->getMessage()));
            }
        }
    }

    /**
     *  Realiza a verificacao do cpf
     *  Encaminha para a atualizacao no sistema
     */
    public function removerUsuario(Request $request)
    {
        try
        {
            //recebendo dados via post
            $arrayDados = $request->all();

            //retorno
            $arrayRetorno = array();

            //pegando cpf do usuario
            $cpf = $arrayDados["cpf"];

            try {
                //removendo usuario
                $deleteUsuario = $this->deleteUsuario($cpf);

                //verificando se deletou
                if ($deleteUsuario['resultado'] == true) {
                    //retorno positivo
                    $arrayRetorno["resultado"] = true;
                    $arrayRetorno["valor"] = array("cpf" => $cpf, "status" => "Removido com sucesso");
                    //retornando
                    return $arrayRetorno;
                } else {
                    //retornando erro
                    return $deleteUsuario;
                }
            } catch (\Exception $e) {
                return array("resultado" => false, "erro" => array("codigo" => "616", "erro" => $e->getMessage()));
            }
        } catch (\Exception $e) {
            if (strstr($e->getMessage(), 'Undefined index: cpf')) {
                //retornando erro
                return array("resultado" => false, "erro" => array("codigo" => "614", "erro" => "CPF nao informado"));
            } else {
                //retornando erro
                return array("resultado" => false, "erro" => array("codigo" => "615", "erro" => $e->getMessage()));
            }
        }
    }

    /**
     *  Realiza a exclusao do usuario no sistema
     */
    public function deleteUsuario($cpf)
    {
        try
        {
            $resultado['resultado'] = true;

            //pesquisando para esse cpf
            $delete = User::where("cpf", "=", $cpf)->first();

            if (isset($delete)) {
                //removendo para este cpf
                $delete->delete();
                return $resultado;
            } else {
                $resultado['resultado'] = false;
                $resultado['erro'] = array("codigo" => "629", "erro" => 'CPF nao cadastrado');
                return $resultado;
            }
        } catch (\Exception $e) {
            //retornando erro
            return array("resultado" => false, "erro" => array("codigo" => "617", "erro" => $e->getMessage()));
        }
    }

    /**
     *  Realiza a pesquisa de um usuario no sistema
     */
    public function pesquisarUsuario($cpf)
    {
        try
        {
            $array_retorno = array();

            //pesquisando
            $pesquisa = User::where("cpf", "=", $cpf)->get()->toArray();

            if (!empty($pesquisa)) {
                $array_retorno["resultado"] = true;
                $array_retorno["valor"] = $pesquisa;
            } else {
                $array_retorno['resultado'] = false;
                $array_retorno['erro'] = array("codigo" => "630", "erro" => 'CPF nao cadastrado');
            }

            return $array_retorno;

        } catch (\Exception $e) {
            //retornando erro
            return array("resultado" => false, "erro" => array("codigo" => "618", "erro" => $e->getMessage()));
        }
    }

    /**
     *  Realiza a pesquisa de um usuario no sistema
     */
    public function pesquisarEmail($email)
    {
        try
        {
            $array_retorno = array();

            //pesquisando
            $pesquisa = User::where("email", "=", $email)->get()->toArray();

            if (!empty($pesquisa)) {
                $array_retorno["resultado"] = true;
                $array_retorno["valor"] = $pesquisa;
            } else {
                $array_retorno['resultado'] = false;
                $array_retorno['erro'] = array("codigo" => "644", "erro" => 'E-mail nao cadastrado');
            }

            return $array_retorno;

        } catch (\Exception $e) {
            //retornando erro
            return array("resultado" => false, "erro" => array("codigo" => "618", "erro" => $e->getMessage()));
        }
    }

    /**
     *  Pesquisa e lista os usuarios cadastrados
     */
    public function listaUsuarios()
    {
        try
        {
            $array_retorno = array();

            //pesquisando
            $pesquisa = User::where("ativo", "=", true)->get(
                [
                    'name', 'email', 'cpf','ativo','avatar'
                ]
            );

            if (!empty($pesquisa)) {
                $array_retorno["resultado"] = true;
                $array_retorno["total"] = count($pesquisa);
                $array_retorno["valor"] = $pesquisa;
            } else {
                $array_retorno['resultado'] = false;
                $array_retorno['erro'] = array("codigo" => "631", "erro" => 'Grupo nao cadastrado');
            }

            return $array_retorno;

        } catch (\Exception $e) {
            //retornando erro
            return array("resultado" => false, "erro" => array("codigo" => "618", "erro" => $e->getMessage()));
        }
    }

    /**
     *  Verifica o acesso do usuario para a página especifica - Back
     */
    public function verificaAcessoUsuario($cpf, $modulo)
    {
        try
        {
            $dados = User::raw(function ($collection) use ($cpf) {
                return $collection->aggregate(
                    [
                        [
                            '$match' => [
                                'cpf' => $cpf,
                            ],
                        ],
                        [
                            '$lookup' => [
                                'as' => 'empresa',
                                'from' => 'grupo_empresas',
                                'foreignField' => 'nome_grupo',
                                'localField' => 'grupo_emp',
                            ],
                        ],
                        [
                            '$unwind' => '$empresa',
                        ],
                        [
                            '$lookup' => [
                                'as' => 'usuario',
                                'from' => 'grupo_usuarios',
                                'foreignField' => 'nome_grupo',
                                'localField' => 'grupo_usuario',
                            ],
                        ],
                        [
                            '$unwind' => '$usuario',
                        ],
                        [
                            '$project' => [
                                'empresas' => '$empresa',
                                'usuarios' => '$usuario',
                            ],
                        ],
                    ]
                );
            })->toArray();
            try {
                if (isset($dados[0])) {
                    $empresa = ($dados[0]['empresas'][$modulo]);
                    $usuario = ($dados[0]['usuarios'][$modulo]);
                    if (($empresa["acesso"] == $usuario["acesso"]) && ($usuario["acesso"] == true)) {
                        $arrayRetorno["resultado"] = true;
                        if (($empresa["edicao"] == $usuario["edicao"]) && ($usuario["edicao"] == true)) {
                            $arrayRetorno["valor"] = array("cpf" => $cpf, "status" => "Acesso e edicao permitidos");
                        } else {
                            $arrayRetorno["valor"] = array("cpf" => $cpf, "status" => "Acesso permitido");
                        }
                    } else {
                        $arrayRetorno['resultado'] = false;
                        $arrayRetorno['erro'] = array("codigo" => "632", "erro" => "Acesso nao permitido");
                    }
                } else {
                    $arrayRetorno['resultado'] = false;
                    $arrayRetorno['erro'] = array("codigo" => "633", "erro" => "CPF nao cadastrado");
                }
                return $arrayRetorno;
            } catch (\Exception $e) {
                if (strstr($e->getMessage(), 'Undefined')) {
                    if (strstr($e->getMessage(), 'cpf')) {
                        return array("resultado" => false, "erro" => array("codigo" => "621", "erro" => "CPF nao informado"));
                    } else {
                        return array("resultado" => false, "erro" => array("codigo" => "622", "erro" => "Modulo nao informado"));
                    }
                } else {
                    return array("resultado" => false, "erro" => array("codigo" => "623", "erro" => $e->getMessage()));
                }
            }
        } catch (\Exception $e) {
            return array("resultado" => false, "erro" => array("codigo" => "620", "erro" => $e->getMessage()));
        }
    }

    /**
     *  Informa para quais paginas o usuario tem acesso - Front
     */
    public function retornaAcessoCpf($cpf)
    {
        try
        {
            $arrayRetorno = array();
            $arrayModulos = array(
                "rota",
                "frete",
                "devolucao",
                "fiscal",
                "dashboard",
                "cadastros",
                "romaneios",
                "protocolos",
                "pesquisar",
                "aplicativo",
                "auditoria",
            );

            $dados = User::raw(function ($collection) use ($cpf) {
                return $collection->aggregate(
                    [
                        [
                            '$match' => [
                                'cpf' => $cpf,
                            ],
                        ],
                        [
                            '$lookup' => [
                                'as' => 'empresa',
                                'from' => 'grupo_empresas',
                                'foreignField' => 'nome_grupo',
                                'localField' => 'grupo_emp',
                            ],
                        ],
                        [
                            '$unwind' => '$empresa',
                        ],
                        [
                            '$lookup' => [
                                'as' => 'usuario',
                                'from' => 'grupo_usuarios',
                                'foreignField' => 'nome_grupo',
                                'localField' => 'grupo_usuario',
                            ],
                        ],
                        [
                            '$unwind' => '$usuario',
                        ],
                        [
                            '$project' => [
                                'empresas' => '$empresa',
                                'usuarios' => '$usuario',
                            ],
                        ],
                    ]
                );
            })->toArray();

            try {
                if (isset($dados[0])) {
                    for ($i = 0; $i < count($arrayModulos); $i++) {
                        $empresa = ($dados[0]['empresas'][$arrayModulos[$i]]);
                        $usuario = ($dados[0]['usuarios'][$arrayModulos[$i]]);
                        if (($empresa == $usuario["ler"]) && ($usuario["ler"] == true)) {
                            $arrayRetorno[$arrayModulos[$i]]["ler"] = true;
                            if (($empresa == $usuario["criar"]) && ($usuario["criar"] == true)) {
                                $arrayRetorno[$arrayModulos[$i]]["criar"] = true;
                            } else {
                                $arrayRetorno[$arrayModulos[$i]]["criar"] = false;
                            }
                            if (($empresa == $usuario["editar"]) && ($usuario["editar"] == true)) {
                                $arrayRetorno[$arrayModulos[$i]]["editar"] = true;
                            } else {
                                $arrayRetorno[$arrayModulos[$i]]["editar"] = false;
                            }
                            if (($empresa == $usuario["remover"]) && ($usuario["remover"] == true)) {
                                $arrayRetorno[$arrayModulos[$i]]["remover"] = true;
                            } else {
                                $arrayRetorno[$arrayModulos[$i]]["remover"] = false;
                            }
                        }
                    }
                } else {
                    $arrayRetorno['resultado'] = false;
                    $arrayRetorno['erro'] = array("codigo" => "634", "erro" => "CPF nao cadastrado");
                }
                return $arrayRetorno;
            } catch (\Exception $e) {
                if (strstr($e->getMessage(), 'Undefined')) {
                    if (strstr($e->getMessage(), 'cpf')) {
                        return array("resultado" => false, "erro" => array("codigo" => "625", "erro" => "CPF nao informado"));
                    }
                } else {
                    return array("resultado" => false, "erro" => array("codigo" => "626", "erro" => $e->getMessage()));
                }
            }
        } catch (\Exception $e) {
            return array("resultado" => false, "erro" => array("codigo" => "624", "erro" => $e->getMessage()));
        }
    }

    //Faz o cadastro da senha de usuarios sem senha cadastrada
    public function cadastroSenha($cpf, $password)
    {

        $resultado['resultado'] = true;

        try {
            //criptografando a senha
            $arrayDados['password'] = Hash::make($password);

            $update = User::where("cpf", "=", $cpf)->whereNull("password")->update($arrayDados);

            if ($update > 0) {
                $resultado['valor'] = array("cpf" => $cpf, "status" => "senha atualizada com sucesso");
            } else {
                $resultado['valor'] = array("cpf" => $cpf, "status" => "senha nao atualizada");
            }
        } catch (Exception $e) {
            $resultado['resultado'] = false;
            if (strstr($e->getMessage(), 'Undefined index: cpf ')) {
                //retornando erro
                return array("resultado" => false, "erro" => array("codigo" => "645", "erro" => "CPF nao informado"));
            } else {
                $resultado['erro'] = array("codigo" => "640", "erro" => $e->getMessage());
            }
            return $resultado;
        }
        return $resultado;
    }

########################### funcoes referentes ao App ###################################################

    public function gerarTokenApp(Request $request)
    {
      
        //inicia variaveis
        $dados = $request->all();
        $data_atual = new \MongoDB\BSON\UTCDateTime((new \DateTime(date('Y-m-d H:i:s')))->getTimestamp() * 1000);

        try {
            //verifica campos
            if (!isset($dados["cpf"])) {
                throw new \Exception("cpf nao enviado");
            }
            if (!isset($dados["imei"])) {
                throw new \Exception("imei nao enviado");
            }

            //pesquisa usuario pelo cpf
            $usuario = User::where("cpf", "=", $dados["cpf"])->first();
            if (empty($usuario)) {
                throw new \Exception("cpf incorreto");
            }
            //se recebeu token
            if (isset($dados["token"])) {
                $token = $this->descriptografarToken($dados["token"],$dados["cpf"]);
                //valida o token 
                if ($token["valor"] == $usuario->token_app) {
                    
                    //cria um token novo
                    $token_novo = Hash::make($dados["cpf"] . $dados["imei"]);
                    $usuario->token_app = $token_novo;
                    $usuario->dt_token_app = $data_atual;
                    $usuario->save();
                } else {
                    throw new \Exception("token_app invalido");
                }
            } else {
               throw new \Exception("token_app invalido");
            }
         
            //criptografa o token
            $tokenCripto = $this->criptografarToken($token_novo,$dados["cpf"]);
            if(!$tokenCripto["resultado"]){
                throw new \Exception("erro na criptografia ".$tokenCripto["valor"]);
            }
            //monta retorno
            $resultado["resultado"] = true;
            $resultado["valor"] = [
                "token_app" => $tokenCripto["valor"],
                "nome" => $usuario->name,
                "cpf" => $usuario->cpf,
                "data_token" => $usuario->dt_token_app,
            ];
        } catch (Exception $e) {
            $resultado["resultado"] = false;
            $resultado["erro"] = $e->getMessage();
        }
 
        return $resultado;
    }

    public function criptografarToken($token,$cpf){
        
        try{
            //inicia variaveis
            $tokenSplit = array();
            $aux = array();
            
            $tamanho = strlen($token)/4;
            if(!is_int($tamanho)){
                $tamanho = intval($tamanho);
            }

            $aux[0] = substr($token, 0, $tamanho);
            $aux[1] = substr($token, $tamanho, $tamanho);
            $aux[2] = substr($token, $tamanho*2, $tamanho);
            $aux[3] = substr($token, $tamanho*3);

            if(substr($cpf,0,1) == "1" || substr($cpf,0,1) == "2" || substr($cpf,0,1) == "3" || substr($cpf,0,1) == "4"){
                $tokenSplit[0] = $aux[1];
                $tokenSplit[1] = $aux[0];
                $tokenSplit[2] = $aux[2];
                $tokenSplit[3] = $aux[3];
            }
            if(substr($cpf,0,1) == "5" || substr($cpf,0,1) == "6" || substr($cpf,0,1) == "7"){
                $tokenSplit[0] = $aux[0];
                $tokenSplit[1] = $aux[2];
                $tokenSplit[2] = $aux[3];
                $tokenSplit[3] = $aux[1];
            }
            if(substr($cpf,0,1) == "8" || substr($cpf,0,1) == "9" || substr($cpf,0,1) == "0"){
                $tokenSplit[0] = $aux[0];
                $tokenSplit[1] = $aux[3];
                $tokenSplit[2] = $aux[1];
                $tokenSplit[3] = $aux[2];
            }

            $resultado["resultado"] = true;
            $resultado["valor"] = $tokenSplit;

        }catch(Exception $e){
            $resultado["resultado"] = false;
            $resultado["valor"] = $e->getMessage();
        }
        return $resultado;
    }

    public function descriptografarToken($tokenSplit, $cpf)
    {
        try {
            if (substr($cpf, 0, 1) == "1" || substr($cpf, 0, 1) == "2" || substr($cpf, 0, 1) == "3" || substr($cpf, 0, 1) == "4") {
                $token = $tokenSplit[1].$tokenSplit[0].$tokenSplit[2].$tokenSplit[3];
            }
            if (substr($cpf, 0, 1) == "5" || substr($cpf, 0, 1) == "6" || substr($cpf, 0, 1) == "7") {
                $token = $tokenSplit[0].$tokenSplit[2].$tokenSplit[3].$tokenSplit[1];
            }
            if (substr($cpf, 0, 1) == "8" || substr($cpf, 0, 1) == "9" || substr($cpf, 0, 1) == "0") {
                $token = $tokenSplit[0].$tokenSplit[3].$tokenSplit[1].$tokenSplit[2];
            }

            $resultado["resultado"] = true;
            $resultado["valor"] = (string) $token;

        } catch (Exception $e) {
            $resultado["resultado"] = false;
            $resultado["valor"] = $e->getMessage();
        }
        return $resultado;
    }

#########################################################################################################
}
